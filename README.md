1) The whole project is responsive;
2) Cross-browser compatibility is ensured by the use of tags which are available in any browser
3) Tags and attributes are used according to their purpose
4) Added sliders of images written in js;
5) In order not to store files with styles for different planets and not to repeat, styles for all planets are combined into one file, Planets.module.scss
6) Site does not allow duplicates and uninformative content to get into the index, loads quickly and does not contain critical errors in the code.