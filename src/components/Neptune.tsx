import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "../neptune1.jpg", "../neptune2.jpg", "../neptune3.jpg"
]

const Neptune = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <div className={styles.photos}>
                <div className={styles.buttons} onClick={() =>{
                    if (currentImage === 0) setCurrentImage(images.length - 1)
                    else
                        setCurrentImage(currentImage - 1);
                }}>Предыдущее</div>
                <img alt="" src={images[currentImage]} className={styles.galImg}/>
                <div className={styles.buttons} onClick={()=>{
                    if (currentImage === images.length - 1) setCurrentImage(0)
                    else
                        setCurrentImage(currentImage + 1)
                }}>Следующее</div>
            </div>
            <div className={styles.info}>
                <cite>
                    Непту́н — восьмая и самая дальняя от Земли планета Солнечной системы.
                    По диаметру находится на четвёртом месте, а по массе — на третьем. Масса Нептуна в 17,2 раза,
                    а диаметр экватора в 3,9 раза больше земных. Планета была названа в честь римского бога морей.
                    Её астрономический символ Neptune — стилизованная версия трезубца Нептуна.
                    Обнаруженный 23 сентября 1846 года, Нептун стал первой планетой, открытой благодаря математическим
                    расчётам. Обнаружение непредвиденных изменений в орбите Урана породило гипотезу о неизвестной планете,
                    гравитационным возмущающим влиянием которой они и обусловлены. Нептун был найден в пределах предсказанного
                    положения. Вскоре был открыт и его спутник Тритон, однако остальные 13 спутников, известные ныне, были
                    неизвестны до XX века. Нептун был посещён лишь одним космическим аппаратом, «Вояджером-2», который
                    пролетел вблизи от планеты 25 августа 1989 года.
                </cite>
            </div>
        </div>
    </div>
}

export default Neptune