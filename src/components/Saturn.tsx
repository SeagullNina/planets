import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "../saturn1.png", "../saturn2.jpg", "../saturn3.jpg"
]

const Saturn = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <div className={styles.photos}>
                <div className={styles.buttons} onClick={() =>{
                    if (currentImage === 0) setCurrentImage(images.length - 1)
                    else
                        setCurrentImage(currentImage - 1);
                }}>Предыдущее</div>
                <img alt="" src={images[currentImage]} className={styles.galImg}/>
                <div className={styles.buttons} onClick={()=>{
                    if (currentImage === images.length - 1) setCurrentImage(0)
                    else
                        setCurrentImage(currentImage + 1)
                }}>Следующее</div>
            </div>
            <div className={styles.info}>
                <cite>
                    Сату́рн — шестая планета от Солнца и вторая по размерам планета в Солнечной системе после Юпитера.
                    Сатурн, а также Юпитер, Уран и Нептун, классифицируются как планеты-гиганты.
                    Сатурн назван в честь римского бога земледелия. Символ Сатурна — серп (Юникод: ♄).
                    В основном Сатурн состоит из водорода, с примесями гелия и следами воды, метана, аммиака и тяжёлых элементов.
                    Внутренняя область представляет собой относительно небольшое ядро из железа, никеля и льда, покрытое тонким
                    слоем металлического водорода и газообразным внешним слоем. Внешняя атмосфера планеты кажется из космоса
                    спокойной и однородной, хотя иногда на ней появляются долговременные образования. Скорость ветра на
                    Сатурне может достигать местами 1800 км/ч, что значительно больше, чем на Юпитере.
                    У Сатурна имеется планетарное магнитное поле, занимающее промежуточное положение по напряжённости
                    между магнитным полем Земли и мощным полем Юпитера.
                </cite>
            </div>
        </div>
    </div>
}

export default Saturn