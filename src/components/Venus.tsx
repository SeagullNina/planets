import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "../venera1.jpg", "../venera2.jpg", "../venera3.jpg"
]

const Venus = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
            <div className={styles.content}>
                <div className={styles.photos}>
                    <div className={styles.buttons} onClick={() =>{
                        if (currentImage === 0) setCurrentImage(images.length - 1)
                        else
                            setCurrentImage(currentImage - 1);
                    }}>Предыдущее</div>
                    <img alt="" src={images[currentImage]} className={styles.galImg}/>
                    <div className={styles.buttons} onClick={()=>{
                        if (currentImage === images.length - 1) setCurrentImage(0)
                        else
                            setCurrentImage(currentImage + 1)
                    }}>Следующее</div>
                </div>
                <div className={styles.info}>
                    <cite>
                        Вене́ра — вторая по удалённости от Солнца планета Солнечной системы, наряду с Меркурием, Землёй и Марсом принадлежащая к семейству планет земной группы. Названа в честь древнеримской богини любви Венеры.
                        По ряду характеристик — например, по массе и размерам — Венера считается «сестрой» Земли.
                        Венерианский год составляет 224,7 земных суток. Она имеет самый длинный период вращения вокруг своей оси
                        (около 243 земных суток, в среднем 243,0212 ± 0,00006 сут) среди всех планет Солнечной системы и вращается в
                        направлении, противоположном направлению вращения большинства планет.
                        Венера не имеет естественных спутников. Это третий по яркости объект на небе Земли, после Солнца и Луны.
                        Планета достигает видимой звёздной величины −4,6m, так что её яркости достаточно, чтобы отбрасывать тени ночью.
                        Изредка Венера видна невооружённым глазом и в светлое время суток.
                    </cite>
                </div>
            </div>
        </div>
}

export default Venus