import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "merk1.jpg", "merk2.jpg", "merk3.jpg"
]

const Merkury = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <div className={styles.photos}>
                <div className={styles.buttons} onClick={() =>{
                    if (currentImage === 0) setCurrentImage(images.length - 1)
                    else
                        setCurrentImage(currentImage - 1);
                }}>Предыдущее</div>
                <img alt="" src={images[currentImage]} className={styles.galImg}/>
                <div className={styles.buttons} onClick={()=>{
                    if (currentImage === images.length - 1) setCurrentImage(0)
                    else
                        setCurrentImage(currentImage + 1)
                }}>Следующее</div>
            </div>
            <div className={styles.info}>
                <cite>
                Мерку́рий — ближайшая к Солнцу планета Солнечной системы, наименьшая из планет земной группы.
                Названа в честь древнеримского бога торговли — быстрого Меркурия, поскольку она движется по небу быстрее других планет.
                Её период обращения вокруг Солнца составляет всего 87,97 дней — самый короткий среди всех планет Солнечной системы.
                Видимое расстояние Меркурия от Солнца, если смотреть с Земли, никогда не превышает 28°.
                Эта близость к Солнцу означает, что планету можно увидеть только в течение небольшого времени после захода или до восхода солнца, обычно в сумерках.
                В телескоп у Меркурия можно увидеть фазы, изменяющиеся от тонкого серпа до почти полного диска, как у Венеры и Луны, а иногда он проходит по диску Солнца.
                Период изменения фаз Меркурия равен синодическому периоду его обращения — примерно 116 дней.
                </cite>
            </div>
        </div>
    </div>
}

export default Merkury