import React from "react";
import styles from "./Navigation.module.scss";
import { NavLink } from "react-router-dom";
import classNames from "classnames";

const Navigation = () => {
  return (
    <div className={styles.mainPage}>
      <div className={styles.navigation}>
        <div className={styles.logoBlock}>
          <img alt="" className={styles.logo} src="../mainLogo.png" />
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/merkury"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Меркурий</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/venus/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Венера</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/earth/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Земля</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
              to="/mars/"
              className={styles.link}
              activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Марс</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
              to="/jupiter/"
              className={styles.link}
              activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Юпитер</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
              to="/saturn/"
              className={styles.link}
              activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Сатурн</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
              to="/uranus/"
              className={styles.link}
              activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Уран</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
              to="/neptune/"
              className={styles.link}
              activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Нептун</span>
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Navigation;
