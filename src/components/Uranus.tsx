import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "../uran1.jpg", "../uran2.jpg", "../uran3.jpg"
]

const Uranus = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <div className={styles.photos}>
                <div className={styles.buttons} onClick={() =>{
                    if (currentImage === 0) setCurrentImage(images.length - 1)
                    else
                        setCurrentImage(currentImage - 1);
                }}>Предыдущее</div>
                <img alt="" src={images[currentImage]} className={styles.galImg}/>
                <div className={styles.buttons} onClick={()=>{
                    if (currentImage === images.length - 1) setCurrentImage(0)
                    else
                        setCurrentImage(currentImage + 1)
                }}>Следующее</div>
            </div>
            <div className={styles.info}>
                <cite>
                    Ура́н — планета Солнечной системы, седьмая по удалённости от Солнца, третья по диаметру и четвёртая по массе.
                    Была открыта в 1781 году английским астрономом Уильямом Гершелем и названа в честь греческого бога неба Урана.
                    Уран стал первой планетой, обнаруженной в Новое время и при помощи телескопа.
                    Его открыл Уильям Гершель 13 марта 1781 года, тем самым впервые со времён античности расширив границы Солнечной системы в глазах человека.
                    Несмотря на то, что порой Уран различим невооружённым глазом, более ранние наблюдатели принимали его за тусклую звезду.
                    В отличие от газовых гигантов — Сатурна и Юпитера, состоящих в основном из водорода и гелия, в недрах Урана и
                    схожего с ним Нептуна отсутствует металлический водород, но зато много льда в его высокотемпературных модификациях.
                    По этой причине специалисты выделили эти две планеты в отдельную категорию «ледяных гигантов». Основу атмосферы Урана
                    составляют водород и гелий. Кроме того, в ней обнаружены следы метана и других углеводородов, а также облака изо льда,
                    твёрдого аммиака и водорода.
                </cite>
            </div>
        </div>
    </div>
}

export default Uranus