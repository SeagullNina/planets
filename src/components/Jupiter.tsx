import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "../jupiter1.jpg", "../jupiter2.png", "../jupiter3.jpg"
]

const Jupiter = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <div className={styles.photos}>
                <div className={styles.buttons} onClick={() =>{
                    if (currentImage === 0) setCurrentImage(images.length - 1)
                    else
                        setCurrentImage(currentImage - 1);
                }}>Предыдущее</div>
                <img alt="" src={images[currentImage]} className={styles.galImg}/>
                <div className={styles.buttons} onClick={()=>{
                    if (currentImage === images.length - 1) setCurrentImage(0)
                    else
                        setCurrentImage(currentImage + 1)
                }}>Следующее</div>
            </div>
            <div className={styles.info}>
                <cite>
                    Юпи́тер — крупнейшая планета Солнечной системы, пятая по удалённости от Солнца.
                    Наряду с Сатурном, Ураном и Нептуном, Юпитер классифицируется как газовый гигант.
                    Планета была известна людям с глубокой древности, что нашло своё отражение в мифологии и религиозных
                    верованиях различных культур: месопотамской, вавилонской, греческой и других. Современное название
                    Юпитера происходит от имени древнеримского верховного бога-громовержца.
                    Ряд атмосферных явлений на Юпитере: штормы, полярные сияния, — имеет масштабы, на порядки превосходящие
                    земные. Примечательным образованием в атмосфере является Большое красное пятно — гигантский шторм,
                    известный с XVII века.
                </cite>
            </div>
        </div>
    </div>
}

export default Jupiter