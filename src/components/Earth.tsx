import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "../earth1.jpg", "../earth2.jpg", "../earth3.jpg"
]

const Earth = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <div className={styles.photos}>
                <div className={styles.buttons} onClick={() =>{
                    if (currentImage === 0) setCurrentImage(images.length - 1)
                    else
                        setCurrentImage(currentImage - 1);
                }}>Предыдущее</div>
                <img alt="" src={images[currentImage]} className={styles.galImg}/>
                <div className={styles.buttons} onClick={()=>{
                    if (currentImage === images.length - 1) setCurrentImage(0)
                    else
                        setCurrentImage(currentImage + 1)
                }}>Следующее</div>
            </div>
            <div className={styles.info}>
                <cite>
                    Земля́ — третья по удалённости от Солнца планета Солнечной системы.
                    Самая плотная, пятая по диаметру и массе среди всех планет и крупнейшая среди планет земной группы,
                    в которую входят также Меркурий, Венера и Марс. Единственное известное человеку на данный момент тело
                    Солнечной системы в частности и Вселенной вообще, населённое живыми организмами.
                    В публицистике и научно-популярной литературе могут использоваться синонимические термины — мир,
                    голубая планета, Терра (от лат. Terra).
                    Научные данные указывают на то, что Земля образовалась из солнечной туманности около 4,54 миллиарда лет
                    назад и вскоре после этого обрела свой единственный естественный спутник — Луну.
                    Жизнь, предположительно, появилась на Земле примерно 4,25 млрд лет назад, то есть вскоре после её
                    возникновения.
                </cite>
            </div>
        </div>
    </div>
}

export default Earth