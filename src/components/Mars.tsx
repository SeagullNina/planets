import React from "react";
import styles from './Planet.module.scss'
import Navigation from "./Navigation";

const images = [
    "../mars1.jpg", "../mars2.png", "../mars3.jpg"
]

const Mars = () => {
    const [currentImage, setCurrentImage] = React.useState(0);
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <div className={styles.photos}>
                <div className={styles.buttons} onClick={() =>{
                    if (currentImage === 0) setCurrentImage(images.length - 1)
                    else
                        setCurrentImage(currentImage - 1);
                }}>Предыдущее</div>
                <img alt="" src={images[currentImage]} className={styles.galImg}/>
                <div className={styles.buttons} onClick={()=>{
                    if (currentImage === images.length - 1) setCurrentImage(0)
                    else
                        setCurrentImage(currentImage + 1)
                }}>Следующее</div>
            </div>
            <div className={styles.info}>
                <cite>
                    Марс — четвёртая по удалённости от Солнца и седьмая по размерам планета Солнечной системы;
                    масса планеты составляет 10,7 % массы Земли.
                    Названа в честь Марса — древнеримского бога войны, соответствующего древнегреческому Аресу.
                    Иногда Марс называют «красной планетой» из-за красноватого оттенка поверхности, придаваемого ей минералом
                    маггемитом — γ-оксидом железа(III).
                    Марс — планета земной группы с разреженной атмосферой (давление у поверхности в 160 раз меньше земного).
                    Особенностями поверхностного рельефа Марса можно считать ударные кратеры наподобие лунных, а также вулканы,
                    долины, пустыни и полярные ледниковые шапки наподобие земных.
                </cite>
            </div>
        </div>
    </div>
}

export default Mars