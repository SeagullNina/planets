import React from 'react'
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom'
import Merkury from './Merkury'
import Venus from "./Venus";
import Earth from "./Earth";
import Mars from "./Mars";
import Jupiter from "./Jupiter";
import Saturn from "./Saturn";
import Uranus from "./Uranus";
import Neptune from "./Neptune";

const Routes = React.memo(() => {
    let log = !!localStorage.getItem('logged');
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/merkury/">
                    <Merkury/>
                </Route>
                <Route path="/venus/">
                    <Venus/>
                </Route>
                <Route path="/earth/">
                    <Earth/>
                </Route>
                <Route path="/mars/">
                    <Mars/>
                </Route>
                <Route path="/jupiter/">
                    <Jupiter/>
                </Route>
                <Route path="/saturn/">
                    <Saturn/>
                </Route>
                <Route path="/uranus/">
                    <Uranus/>
                </Route>
                <Route path="/neptune/">
                    <Neptune/>
                </Route>
            </Switch>
        </BrowserRouter>
    )
})

export default Routes
